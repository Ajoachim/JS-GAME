"use strict";

let btnNomAnimal = document.querySelector('#btn-animal');
let choix = '';
btnNomAnimal.addEventListener('click', function () {
  let destination = document.querySelector('#target-phrase');
  destination.innerHTML = '';
  destination.appendChild(randomPhrase());
})

//mettre les fonctions à l'extérieur de l'addEventListener
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function randomPhrase() {
  let phrase = document.createElement('div');
  phrase.classList.add('phrase'); //on ajoute une class qui s'appelle phrase à la div

  let listePhrase = [
    { nom: "Simone la Girafe", image: "../picture/img_1.jpg", img: "../picture/lion.jpg"}, //objet
    { nom: "Jack le Gorille", image: "../picture/img_2.jpg", img: "../picture/chat.jpg" },
    { nom: "Gisèle le Panda", image: "../picture/img_3.jpg", img: "../picture/bbgirafe.jpg" }
  ];
  choix = listePhrase[getRandomInt(0, listePhrase.length - 1)];

  phrase.innerHTML = choix.nom;


  return phrase;


}
// BOUTON PLAY
let btnready = document.querySelector('.btn-ready');
let model = document.querySelector('.model')
let play = document.querySelector('.play');
let ready = document.querySelector('.ready');

btnready.addEventListener('click', function () {
 
   //AJOUT DU CHRONO DOM
   function progress(timeLimit) {
    let main = document.querySelector('main');

    let progressbar =  document.createElement('div');
    progressbar.setAttribute("class", "progress-bar");
    main.appendChild(progressbar);
   
    let prg = document.createElement('div');
    prg.setAttribute("class", "progress");
    progressbar.appendChild(prg);
    // let prg = document.querySelector('.progress');
    
    let curentTime = 0;
  
    let id = setInterval(frame, 50);//50
  
    function frame() {
      curentTime += 50;
      if (curentTime >= timeLimit) {
        clearInterval(id);
      }
      prg.style.width = ((curentTime/timeLimit) *100)+"%";
    }
  }
  progress(180000);//on rentre des millisecondes
 
 
 
 
 
 
 
  //   //  if ( randomPhrase() === "Simone la Girafe"){

  //   //  }
  let img1 = document.createElement('img');
  img1.src = choix.image;
  model.innerHTML ="";
  model.appendChild(img1);
  
  let img2 =document.createElement('img');
  img2.src = choix.img;
  
  play.innerHTML ="";
  play.appendChild(img2);
 





})

// CHRONOMETRE
// function progress(timeLimit) {
//   let prg = document.querySelector('.progress');
  
//   let curentTime = 0;

//   let id = setInterval(frame, 50);//50

//   function frame() {
//     curentTime += 50;
//     if (curentTime >= timeLimit) {
//       clearInterval(id);
//     }
//     prg.style.width = ((curentTime/timeLimit) *100)+"%";
//   }
// }
// progress(180000);//on rentre des millisecondes
